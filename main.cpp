#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

#include "functions.h"
using namespace std;

int main() {
    //1 - Player 1 write a word
    string wordToGuess;
    wordToGuess = getPlayerOneWord(wordToGuess);
    //2 - Programme shuffle word's chars
    string shuffledWord = shuffleThisWord(wordToGuess, shuffledWord);
    //3 - Player 2 try to guess the word, if he's wrong : (3) again
    while(!(playerTwoTry(wordToGuess,shuffledWord))){
        cout << "Ce n'est pas le mot !" << endl;
    }
    //4 - Programme show a 'game over' screen
    cout << "Bravo !\n Game Over.";
    return 0;
}

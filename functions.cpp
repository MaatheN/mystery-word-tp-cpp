//
// Created by Tatadmound on 27/04/2021.
//

#include <string>
#include <iostream>
#include "functions.h"

using namespace std;

string getPlayerOneWord(string &wordToGuess){
    cout << "Saisissez un mot" << endl;
    cin >> wordToGuess;
    return wordToGuess;
}

string shuffleThisWord(string wordToGuess, string &shuffledWord){
    int randomCharIndex(0), wordToGuessSize=wordToGuess.size();
    char relatedChar(0);
    srand(time(0));

    do {
        //1- pick a random position in the word
        randomCharIndex = rand() % wordToGuess.size();
        //2- find is related char
        relatedChar = wordToGuess[randomCharIndex];
        //3- put it in shuffledWord' string
        shuffledWord.append(1,relatedChar);
        //4- make sur to dont pick this random position again
        wordToGuess.erase(randomCharIndex,1);
        //5- check if there is still some chars to add, if yes -> (1)
    } while (wordToGuessSize>shuffledWord.size());

    //6- return the shuffledWord string
    return shuffledWord;

}

bool playerTwoTry(string &wordToGuess, string &shuffledWord){
    string playersTry;
    cout << "Quel est ce mot?" << endl << shuffledWord << endl;
    cin >> playersTry;
    int delta = wordToGuess.compare(playersTry);
    if (delta==0){
        return true;
    }else
        return false;
}
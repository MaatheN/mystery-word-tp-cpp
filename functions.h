//
// Created by Tatadmound on 27/04/2021.
//

#ifndef TP001_OC_FUNCTIONS_H
#define TP001_OC_FUNCTIONS_H

std::string getPlayerOneWord(std::string &wordToGuess);
std::string shuffleThisWord(std::string wordToGuess, std::string &shuffledWord);
bool playerTwoTry(std::string &wordToGuess, std::string &shuffledWord);
#endif //TP001_OC_FUNCTIONS_H
